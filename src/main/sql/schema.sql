
-- 创建 数据库
CREATE DATABASE IF NOT EXISTS seckill default character set utf8 COLLATE utf8_general_ci;

use seckill;
DROP TABLE IF EXISTS seckill.t_seckill;
-- 创建秒杀库存表
CREATE TABLE seckill.t_seckill (
  `id` BIGINT NOT NULL AUTO_INCREMENT COMMENT '秒杀库存ID',
  `name` VARCHAR(120) NOT NULL COMMENT '秒杀商品名称',
  `number` int NOT NULL  COMMENT '库存数量',
  `startTime` DATETIME NOT NULL COMMENT '秒杀开始时间',
  `endTime` DATETIME NOT NULL COMMENT '秒杀结束时间',
  `createTime` DATETIME NOT NULL DEFAULT NOW() COMMENT '秒杀创建时间',
  PRIMARY KEY (`id`),
  KEY idx_start_time(`startTime`),
  KEY idx_end_time(`endTime`),
  KEY ids_create_time(`createTime`)
) ENGINE=InnoDB AUTO_INCREMENT=1000 DEFAULT CHARSET=utf8 COMMENT='秒杀库存表';

-- 初始化数据
INSERT INTO
  seckill.t_seckill (name, number, startTime, endTime)
VALUES
  ('1000元秒杀iphone6s', 100, '2016-08-01 00:00:00', '2016-08-02 00:00:00'),
  ('500元秒杀ipad2', 200, '2016-08-01 00:00:00', '2016-08-02 00:00:00'),
  ('300元秒杀小米4', 300, '2016-08-01 00:00:00', '2016-08-02 00:00:00'),
  ('200元秒杀红米Note', 400, '2016-08-01 00:00:00', '2016-08-02 00:00:00');


-- 秒杀成功明细表
DROP TABLE IF EXISTS seckill.t_success_killed;
CREATE TABLE IF NOT EXISTS seckill.t_success_killed (
  `seckillId` BIGINT NOT NULL COMMENT '秒杀商品ID',
  `userPhone` BIGINT NOT NULL COMMENT '用户手机号',
  `state` TINYINT NOT NULL DEFAULT -1 COMMENT '状态标识:-1:无效 0:成功 1:已付款 2:已发货',
  `createTime` DATETIME NOT NULL COMMENT '创建时间',
  PRIMARY KEY(seckillId, userPhone),/*联合主键*/
  KEY idx_create_time(createTime)
) ENGINE=InnoDB DEFAULT CHARSET = utf8 COMMENT = '秒杀成功明细表';